var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var resultData = {};
    resultData.posts = res.locals.db.getCollection('posts').find({'$or': [
            {'content': {'$containsAny': req.query.terms.split(' ')}},
            {'comments.content': {'$containsAny': req.query.terms.split(' ')}}
    ]});
    res.render('results', { results: resultData });
});

module.exports = router;
