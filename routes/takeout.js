var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    var postCount = res.locals.db.getCollection('posts').size;
    var collectionCount = res.locals.db.getCollection('collections').size;
    var activityLogCount = res.locals.db.getCollection('activityLog').size;
    var eventCount = res.locals.db.getCollection('events').size;

    res.render('takeout', { postCount: postCount, collectionCount : collectionCount,
                            activityLogCount : activityLogCount, eventCount : eventCount });
});

router.get('/:fileId', function(req, res) {
    var postData = res.locals.db.getCollection('posts').find({'id': req.params.fileId});
    res.render('post', { post: postData[0] });
});

module.exports = router;