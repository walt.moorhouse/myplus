var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    var collectionData = res.locals.db.getCollection('collections').chain().simplesort('id').data();
    res.render('collections', { collections: collectionData });
});

router.get('/:fileId', function(req, res) {
    var collectionData = res.locals.db.getCollection('collections').find({'id': req.params.fileId});
    var postData =  res.locals.db.getCollection('posts')
        .find({'postAcl.collectionAcl.collection.resourceName':
                'collections'+collectionData[0].resourceName.substring(collectionData[0].resourceName.lastIndexOf('/'))});
    res.render('collection', { collection: collectionData[0], posts: postData });
});

module.exports = router;
