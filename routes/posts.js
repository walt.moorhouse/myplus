var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    var postData = res.locals.db.getCollection('posts').chain().simplesort('creationDate', {desc: true}).data();
    res.render('posts', { posts: postData });
});

router.get('/:fileId', function(req, res) {
    var postData = res.locals.db.getCollection('posts').find({'id': req.params.fileId});
    res.render('post', { post: postData[0] });
});

module.exports = router;
