var express = require('express');
var router = express.Router();

var fs = require('fs');
var path = require('path');

/* GET home page. */
router.get('/', function(req, res) {
    res.send("Not yet implemented.");
});

router.get(/^\/.*\/Google\+ Stream\/Photos\/?$/, function (req, res) {
    var rootDir = res.locals.myConfig.takeout.directory;
    if (rootDir.indexOf('*') >= 0) {
        rootDir = rootDir.substring(0, rootDir.indexOf('*'));
    }
    var path = req.params[0];
    if (path.substring(0, 2) === "..") {
        res.render('403');
    } else {
        var imgLoc = path.join(rootDir, path);
        res.sendFile(imgLoc);
        // var stream = fs.createReadStream(imgLoc);
        //
        // stream.on('error', function (error) {
        //     res.status(500);
        //     res.render('errors/500', {msg: "Can't read Google Takeout Photo: "+req.params[0], err: error});
        // });
        //
        // res.header("Content-Type", "image/jpeg");
        // stream.pipe(res);
    }
});

module.exports = router;
