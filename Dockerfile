FROM alpine:latest

RUN apk add --update --upgrade nfs-utils nodejs nodejs-npm

RUN mkdir /data

RUN sed 's/flock -e/flock -x/g' /usr/sbin/start-statd > /usr/sbin/start-statd.new && rm /usr/sbin/start-statd && mv /usr/sbin/start-statd.new /usr/sbin/start-statd

WORKDIR /app

COPY package*.json ./

RUN npm install --only=production

# Base layer now complete, we can copy the app code.

COPY . .

ENV NODE_ENV production

EXPOSE 80

ENTRYPOINT "./bin/www"
