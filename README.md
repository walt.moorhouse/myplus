# myPlus - self service G+ content

## How do I make this do stuff?
1. Go to [Google Takeout](https://takeout.google.com/) 
2. Click Select None, to unselect all products.
3. Scroll down to Google+ Stream and select it for archive.
4. Click the dropdown and change the file formats from HTML to JSON.
5. Click Next and wait for the files to be created.
6. Download the files and unzip them somewhere. 
7. Change the takeout.directory property in the config/myPlus-config.yaml file to where you unzipped the files.
8. Run bin/www to start the NodeJS and it will load the files from the Takeout directories into the app's database.
9. The app uses an in-memory database, so will reload the files each time it starts up, so keep them handy.
10. For development work, you might want to load a small subset of your files, to keep fast startup times while developing.


## Known Issues

If having an issue with Error: ENFILE: file table overflow on Mac, run the below commands to correct the issue:
```bash
$ echo kern.maxfiles=65536 | sudo tee -a /etc/sysctl.conf
$ echo kern.maxfilesperproc=65536 | sudo tee -a /etc/sysctl.conf
$ sudo sysctl -w kern.maxfiles=65536
$ sudo sysctl -w kern.maxfilesperproc=65536
$ ulimit -n 65536
```

## TODO

1. Load zip files page, if you are deploying in a cloud.
2. Way to post new content.
3. Chat messaging (websockets)
4. Way to connect myPlus "pods" together.