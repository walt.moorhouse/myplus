var express = require('express');
              require('pug');
var path = require('path');
var cookieParser = require('cookie-parser');
var requestLogger = require('morgan');
var logger = require('winston');
var sassMiddleware = require('node-sass-middleware');

var fs = require('fs');
var glob = require( 'glob' );
var yaml_config = require('node-yaml-config');
var config = yaml_config.load(__dirname + '/config/myPlus-config.yaml');

logger.level = 'debug';
logger.info("Configuration: "+JSON.stringify(config));

var TAKEOUT_DIR = config.takeout.directory;

var loki = require('lokijs');
var db = new loki('myPlusDB.json');
var posts = db.addCollection('posts', { indices: ['creationTime']});
var collections = db.addCollection('collections');
var activityLog = db.addCollection('activityLog');
var events = db.addCollection('events');

function initDataBase() {
    if (config.takeout.load.posts)
        loadCollection(posts, TAKEOUT_DIR+'Posts/');
    if (config.takeout.load.collections)
        loadCollection(collections, TAKEOUT_DIR+'Collections/');
    if (config.takeout.load.activityLog)
        loadCollection(activityLog, TAKEOUT_DIR+'ActivityLog/');
    if (config.takeout.load.events)
        loadCollection(events, TAKEOUT_DIR+'Events/');
}

function loadCollection(collection, directory) {
    glob(directory+'**/*.json', function (err, files) {
        if (err) {
            console.error(err);
            return;
        }
        files.forEach(function (filename) {
            fs.readFile(filename, 'utf-8', function (err, content) {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log("Reading '" + filename + "' into DB.");
                var obj = JSON.parse(content);
                obj.id = filename.substring(filename.lastIndexOf('/')+1, filename.lastIndexOf('.'));
                collection.insert(obj);
            });
        });
    });
}

var app = express();
app.set('port', config.server.port);
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// Add DB and Config to locals
app.use(function(req, res, next) {
    res.locals.myConfig = config;
    res.locals.db = db;
    next();
});
app.use(requestLogger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    res.redirect('/posts');
});

app.use('/activities', require('./routes/activities'));
app.use('/collections', require('./routes/collections'));
app.use('/events', require('./routes/events'));
app.use('/photos', require('./routes/photos'));
app.use('/posts', require('./routes/posts'));
app.use('/search', require('./routes/search'));

app.use(function errorHandler(err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(404);
    res.render('errors/404', {error: "Can't find Google Takeout File..."});
});

initDataBase();

module.exports = app;
